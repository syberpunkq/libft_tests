#include <stdio.h>
#include "../libft/ft_isdigit.c"
int main(void)
{
    char c = 'a';
    printf("result of isdigit for %c is %d\n", c, ft_isdigit(c));
    c = '0';
    printf("result of isdigit for %c is %d\n", c, ft_isdigit(c));

    return(0);
}
