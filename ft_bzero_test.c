#include <stdio.h>
#include "../libft/ft_bzero.c"

int main(void)
{
    
    char test[] = "salut";
    int i;
    printf("String test = salut\n");
    for (i = 0; i < 5; i++){
        printf("%d\n", test[i]);
    }
    printf("calling ft_bzero(void *, size_t)\n");
    ft_bzero(test, 5);
    for (i = 0; i < 5; i++){
        printf("%d\n", test[i]);
    }
	printf("=== end ft_bzero ===\n");
    return (0);
}

