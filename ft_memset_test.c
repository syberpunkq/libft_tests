#include <stdio.h>
#include "../libft/ft_memset.c"

int main(void)
{
    
    char test[] = "salut";
    int i;
    printf("String test = salut\n");
    for (i = 0; i < 5; i++){
        printf("%d\n", test[i]);
    }
    printf("calling ft_memset(void *, int, size_t)\n");
    ft_memset(test, 'a', 3);
    for (i = 0; i < 5; i++){
        printf("%d\n", test[i]);
    }
	printf("=== end ft_memset ===\n");
    return (0);
}

